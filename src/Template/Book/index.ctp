<div class="container">  

<div class="panel panel-primary">
  <div class="panel-heading">Book List   
  
    <?php  
      echo $this->Html->link(
        " + Add Book" ,
        "/create",
        [
          "class"=>"btn btn-success pull-right",
          "style"=>"margin-top:-6px ;"
        ]
        );
        ?>
  </div>

    <div class="panel-body">

    <table class="table table-bordered">
    <thead>
      <tr>
        <th>SR NO: </th>
        <th>Name</th>
        <th>Author Name </th>
        <th>Email</th>
        <th>Action </th>
      </tr>
    </thead>
    <tbody>

    <?php   

      $count = 1;
      foreach($books as $key=>$book) {
     ?>  
       <tr>
        <td> <?= $count++ ?></td>
        <td><?= $book->name ?></td>
        
        <td><?= $book->author ?></td>
        <td><?= $book->email ?></td>

         <td>  
         <?php   

          echo $this->Html->link( 
            "Edit" ,
            "book/edit".$book->id,
            [  
              "class"=>"btn btn-info "

            ]

            );


            echo $this->Html->link( 
              "Delete" ,
              "book/delete".$book->id,
              [  
                "class"=>"btn btn-danger ",
                "style"=>"margin-left:5px"
  
              ]
  
              );
         
         
         ?>

       
   
        </td>
      </tr>

     <?php 
      

      }
    
    ?>

    
      
    </tbody>
  </table>
    
    
    </div>
</div>

</div>  
