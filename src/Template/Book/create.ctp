<div class="panel panel-primary">

<div class="panel-heading">Book Create </div>
  <div class="panel-body">
 
    <form  method="POST" action="book/save">

        <div class="form-group">
            <label for="name">Name : </label>
            <input type="text" class="form-control" id="name" name="name"  >
        </div>

        <div class="form-group">
            <label for="author">Author Name : </label>
            <input type="text" class="form-control" id="author" name="author"  >
        </div>

        <div class="form-group">
            <label for="name">Email : </label>
            <input type="email" class="form-control" id="email" name="email"  >
        </div>

        <div class="form-group">
            <label for="description">Description : </label>
            <input type="text" class="form-control" id="description" name="description"  >
        </div>

        
        
        

    <button type="submit" class="btn btn-primary">Submit</button>
    </form> 
  
  
  
  </div>
</div>