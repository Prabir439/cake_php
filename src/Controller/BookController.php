<?php 

    namespace App\Controller;

    use App\Controller\AppController;

    class BookController extends AppController
    {

        public function initialize()
        {
            parent::initialize();

         
            

            $this->loadModel("Books");

            $this->viewBuilder()->layout("booklayout");

        }

        public function index()
        {   
            $books = $this->Books->find("all", 
            [
                "order"=>["id"=>"desc"]
            ]);

            $this->set("title","Book List");

            $this->set("books",$books);
        }



        public function create()
        {
            $this->set("title" , "Book Create");
        }

        public function save()
        {
            $this->autoRender = false;
           
            print_r($this->request->data);
           
            
        }












       public function edit($id)
       {
           $this->set("title","Book Edit");
       } 


       public function update()
       {
           $this->set("title","Book Update");
       } 

       public function delete($id)
       {
           $this->set("title","Book delete");
       } 

    }


?>